# dtrelease - the debian metapackage

dtrelease is a proof-of-concept project which will hopefully be used to replace many of the mundane and error prone upgrade procedures which we currently have to perform in order to move to a new release of software.

We are using the dragontail debian repository to host the built *.deb*, the idea is that support can 'apt-get install dtrelease=1.4' to upgrade to DOM 1.4 instead of manually pulling each package and managing the configuration files, stopping of services, etc.

